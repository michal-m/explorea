from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from .forms import RegisterForm, EditProfileForm

# Create your views here.

def profile(request):
	return render(request, 'accounts/profile.html')


def register(request):

	if request.method == 'POST':
		# create the form and populate it with data from the POST request
		form = RegisterForm(request.POST)
		
		# validate the data in the form
		if form.is_valid():
			user = form.save()
			raw_password = form.cleaned_data.get('password1')
			
			# authenticate the user so he is logged in after the registration
			user = authenticate(username=user.username, password=raw_password)
			login(request, user)
			
			# redirect to another page
			return redirect('profile')

	# if the request is GET
	form = RegisterForm()
	return render(request, 'accounts/register.html', {'form': form} )


def edit_profile(request):
	if request.method == 'POST':
		form = EditProfileForm(request.POST, instance=request.user)

		if form.is_valid():
			user = form.save()
			return redirect('profile')

	form = EditProfileForm(instance=request.user)
	return render(request, 'accounts/edit_profile.html', {'form': form} )
