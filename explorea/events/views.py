from django.shortcuts import render, redirect
from .models import Event
from .models import EventRun
from .forms import EventForm

# Create your views here.
from django.http import HttpResponse

def event_create(request):
	# if POST
	if request.method == 'POST':
		form = EventForm(request.POST)
		
		# validate the data in the form
		if form.is_valid():
			new_event = form.save(commit=False)
			new_event.host = request.user
			new_event.save()
			# redirect to another page
			return redirect('events')
			
	# if GET
	form = EventForm()
	return render(request, 'events/event_create.html', {'form': form} )

def event_update(request, ev_id):
	selected_event = Event.objects.get(pk=ev_id)
	
	# if POST
	if request.method == 'POST':
		if selected_event:
			form = EventForm(request.POST, instance=selected_event)
			if form.is_valid():
				form.save()
			return redirect('/events/' + ev_id)
	
	# if GET
	if selected_event:
		form = EventForm(instance=selected_event)
		return render(request, 'events/event_update.html', {'form':form} )
	else:
		return HttpResponse('You are trying to update an event that does not exist!')
		
def event_delete(request, ev_id):
	selected_event = Event.objects.get(pk=ev_id)

	# if POST
	if request.method == 'POST':
		if selected_event:
			selected_event.delete()
			return redirect('events')
	
	# if GET
	if selected_event:
		selected_event_runs = EventRun.objects.filter(event_id=ev_id)
		return render(request, 'events/event_delete.html', {'selected_event':selected_event, 'selected_event_runs':selected_event_runs})
			
	else:
		return HttpResponse('No such event in offering for now')
		

	
		
def my_events(request):
	my_events = Event.objects.filter(host=request.user)
	if my_events:
		return render(request, 'events/my_events.html', {'my_events':my_events})
	else:
		return HttpResponse('You did not create any events yet')

def index(request):
	return render(request, 'events/index.html')


def event_listing(request):
	events = Event.objects.all()
	
	return render(request, 'events/event_listing.html', {'events': events})


def event_runs_listing(request):	
	event_runs = EventRun.objects.all()
	
	return render(request, 'events/event_runs_listing.html', {'event_runs': event_runs})
	

def event_detail(request, ev_id):
	selected_event = Event.objects.get(pk=ev_id)

	if selected_event:
		selected_event_runs = EventRun.objects.filter(event_id=ev_id)
		return render(request, 'events/event_detail.html', {'selected_event':selected_event, 'selected_event_runs':selected_event_runs})
			
	else:
		return HttpResponse('No such event in offering for now')

