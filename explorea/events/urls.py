from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
	path('events/', views.event_listing, name='events'),
	path('events/create/', views.event_create, name='event_create'),
	path('events/update/<str:ev_id>', views.event_update, name='event_update'),
	path('events/delete/<str:ev_id>', views.event_delete, name='event_delete'),
	path('events/my/', views.my_events, name='my_events'),
	path('event_runs/', views.event_runs_listing, name='event_runs'),
	path('events/<str:ev_id>', views.event_detail, name='detail'),
	
]